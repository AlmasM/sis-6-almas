package Repositories
import Domains.Order
import slick.jdbc.PostgresProfile.api._

import scala.language.postfixOps

class OrderPostgres {
  class Orders(tag: Tag) extends Table[Order](tag, "orders"){
    def id = column[String]("id")
    def address = column[String]("address")
    def details = column[String]("details")
    def userId = column[String]("userId")
    def distance = column[Option[Float]]("distance")
    def * = (id, address, details, userId, distance)<>(Order.tupled, Order unapply(_))
  }
  val orderTableQuery = TableQuery[Orders]

  def insert(order: Order) ={
    orderTableQuery returning orderTableQuery.map(o => o) += order
  }
  def update(orderId:String, distance: Option[Float]) ={
    val q  = for {o <- orderTableQuery  if o.id === orderId} yield o.distance
    q.update(distance)
  }
  def select(orderId: String) ={
    orderTableQuery.filter(_.id === orderId).result
  }
  def delete(orderId: String) ={
    orderTableQuery.filter(_.id === orderId).delete
  }
}
