package migrations

import org.flywaydb.core.Flyway

object Migration {

  private val flyway = new Flyway()
  flyway.setDataSource("jdbc:postgresql://127.0.0.1/orders", "postgres", "changeme")

  def migrate() = {
    flyway.migrate()
  }

  def reloadSchema() = {
    flyway.clean()
    flyway.migrate()
  }
}

